=== URA ===
Tags: URA, payments, payment gateway, woocommerce
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Stable tag: 1.0
Tested up to: 6.2
Requires PHP: 7.1

Accept payments with URA

== Description ==

URA payments module for WooCommerce

== Changelog ==

Initial release
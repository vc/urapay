<?php

/**
 * Plugin Name: URA payments
 * Plugin URI: /
 * Description: URA payments for WooCommerce
 * Version: 1.0
 * License: GPLv2 or later
 * Text Domain: ura-payments
 * Domain Path: /languages/
 */

if (!defined('ABSPATH')) {
    exit;
}

// define constants
define('URAPAYMENTS_PLUGIN_DIR_PATH', untrailingslashit(plugin_dir_path(__FILE__)));
define('URAPAYMENTS_PLUGIN_DIR_URL', untrailingslashit(plugin_dir_url(__FILE__)));

// include gateway class
require_once(URAPAYMENTS_PLUGIN_DIR_PATH.'/src/class.php');

// add link to settings on plugin page
add_filter( 'plugin_action_links_'.plugin_basename(__FILE__), function($links) {
  $links[] = '<a href="'.admin_url( 'admin.php?page=wc-settings&tab=checkout&section=urapay').'">'. __('Settings', 'ura-payments') . '</a>';
  return $links;
});

// register class as an WooCommerce payment gateway
add_filter( 'woocommerce_payment_gateways', function($gateways) {
  $gateways[] = 'WC_Gateway_URAPAY';
  return $gateways;
});

// load text-domain
add_action('init', function() {
  load_plugin_textdomain('ura-payments', false, 'ura-payments/languages');
});

// load plugin
add_action('plugins_loaded', 'wooce_payment_gateway_init');

// register activation hook
register_activation_hook( __FILE__, function() {
  if (!class_exists('WooCommerce')) {
    die(__('WooCommerce is not activated.', 'ura-payments'));
  }
});


  